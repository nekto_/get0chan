# Создано в 2017 году
# Лицензия: MIT License
# Автор говнокода: nekto@0city.ru
# Соавтор/рефакторинг: ugubok (с небольшими коррективами от nekto)
# Ссылка на хранилище: https://bitbucket.org/nekto_/get0chan/src

# Требуется установка requests, pyblake2
# pip install requests
# pip install pyblake2
# pip install mmh3
# если на винде надо собрать в другой студии, например 14-ой
# перед запуском pip ввести set VS100COMNTOOLS=%VS140COMNTOOLS% 


# TODO:
# Список скаченных картинок чтобы не перекачивать что было удалено вручную + проверка дубликатов
# Многопоточность
# Скачивание только новых данных (API предоставляет параметр &after=видимо_номер_сообщения), возможно для этого придётся притащить sqlite
# Переработать информирующие строки и добавить где требуется flush
# Возможно нужен ещё один рефакторинг


import glob
import json
import re
import requests
import sys
from datetime import datetime
from os import path, makedirs, remove, rename
from pyblake2 import blake2b

API_URL = "https://0chan.hk/api/"

ROOT_DIR = path.join('.', '0chan')
BOARDS_FILENAME = path.join(ROOT_DIR, "boards.json")
IMAGES_FILENAME = path.join(ROOT_DIR, "images.json")
BOARDS_DIR = "boards"
IMAGES_DIR = "images"
FILE_CODEPAGE = "utf-8" # encoding, иначе сука пытается сохранить в 1251 и сасёт
DATETIME_FORMAT = "%y.%m.%d -- %H-%M-%S" # https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior

# Вести ли логи в файл
LOG_FILE_WRITE = True

# Сохранять ли картинки
IMAGES_SAVE = True
# Проверять ли на повторы (по хэшу)
IMAGES_CHECK_DUPLICATES = True
IMAGES_CHECK_DUPLICATES_EXISTS_FILES = True

# Обычные доски
# Строка "auto" — все доски доступные с вашего IP, 
# Массив ["a", ...] желаемых досок // если доска недоступна с вашего IP вам это не поможет
BOARDS = "auto"

# Скрытые доски. Имеет смысл указывать только при режиме BOARDS = "auto".
# В противном случае можно вписать прямо в BOARDS.
HIDDEN_BOARDS = [] # ["a", "b", "c", "d"]


# Логирование
def log_start():
	log_filename = path.join(ROOT_DIR, "get0chan #%s.log" % datetime.now().strftime(DATETIME_FORMAT))
	log_directory = path.dirname(log_filename)
	if not path.exists(log_directory):
		makedirs(log_directory)
	return open(log_filename, 'w+', encoding=FILE_CODEPAGE)

def log_write(text, end="\n"):
	print(text, end=end)
	if LOG_FILE_WRITE:
		print(text, end=end, file=log_file)

# Файл со списком картинок
def images_start():
	if path.isfile(IMAGES_FILENAME):
		images_data = file_JSON_read(IMAGES_FILENAME)
	else:
		images_data = {}
	return images_data

# Однократная запись в файл
def file_write(filename, data, mode="text"):
	# говно тупое само не умеет создавать пути
	dir = path.dirname(filename)
	if not path.exists(dir):
		makedirs(dir)
	if mode == "text":
		file = open(filename, 'tw', encoding=FILE_CODEPAGE)
	else:
		file = open(filename, "bw")
	file.write(data)
	file.close()

# Чтение JSON файла
def file_JSON_read(file_path):
	file = open(file_path, mode="tr", encoding=FILE_CODEPAGE)
	data = file.read()
	file.close()
	try:
		json_data = json.loads(data)
	except ValueError:
		log_write("Ошибка! В файле %s обнаружен мусор вместо JSON-данных!" % (file_path))
		log_write("Переименование файла в имя с окончанием \".%дата%.bad\" для дальнешего ручного анализа и повторная попытка скачки...")
		#remove(file_path)
		rename(file_path, file_path + "." + datetime.now().strftime(DATETIME_FORMAT) + ".bad")
		return False
	return json_data


# Получение изображений
def get_images(thread_data, start_idt=0, start_ida=0):
	log_write("Поиск изображений...")
	for idt, thread in enumerate(thread_data['posts'], start_idt):
		for ida, attachment in enumerate(thread['attachments'], start_ida):
			url = "https:" + attachment['images']['original']['url']
			pattern = re.compile("//(.*?0chan\.hk.*?)\?hash=.*?&exp=\d{10}")
			result = re.search(pattern, url)
			if result:
				log_write("Найдено в сообщении %s:" % (thread['id']))
				filepath = result.group(1)
				image_fname = path.join(ROOT_DIR, IMAGES_DIR, filepath)
				if path.isfile(image_fname):
					log_write("Изображение %s уже скачено, путь: %s" % (url, image_fname))
					""" Надо базу прикрутить чтоб по разным столбцам обращаться
					if IMAGES_CHECK_DUPLICATES_EXISTS_FILES:
						file_image = open(image_fname, mode="rb")
						image_data = file_image.read()
						file_image.close()
						hash = blake2b(digest_size=32)
						hash.update(image_data)
						filename = path.basename(image_fname)
						hash = hash.hexdigest()
						if not hash in images_data.keys():
							images_data[hash] = {}
						images_data[hash][filename] = filepath
					"""
				else:
					log_write("Скачивание: " + url, "")
					request = requests.get(url, stream=True)
					if request_ok(request):
						file_write(image_fname, request.raw.read(), "binary")
						log_write(" закончилось.")
					else:
						get_images(thread_data, idt, ida)
			else:
				log_write("Ошибка в разборе ссылки: " + url)

# Получение темы
def get_thread(board, tid, now_downloaded=False):
	thread_filename = path.join(ROOT_DIR, BOARDS_DIR, board, tid + '.json')
	if path.isfile(thread_filename):
		if not now_downloaded:
			log_write("Тема %s уже скачена, удалите файл %s чтоб перекачать." % (tid, thread_filename))
		else:
			log_write("Файл темы: %s" % (thread_filename))
		thread_data = file_JSON_read(thread_filename)
		if not thread_data:
			get_thread(board, tid)
		if IMAGES_SAVE:
			log_write("Данные темы " + tid + ":") # потом какую-то хоть статистику давать
			get_images(thread_data)
	else:
		log_write("Скачиваем тему %s, доски %s..." % (tid, board))
		link = API_URL + 'thread?thread=' + tid + '&session=' + SESSION
		log_write("Скачивание: " + link, "")
		request = requests.get(link)
		if request_ok(request):
			file_write(thread_filename, request.text)
			log_write(" закончено. ", "")
		get_thread(board, tid, True)

# Получение списков тем
def get_threads_list(board, cur_page_num, now_downloaded=False):
	global cur_cursor
	board_threads_fname = path.join(ROOT_DIR, BOARDS_DIR, board, 'threads-page%s.json' % cur_page_num)
	if path.isfile(board_threads_fname):
		if not now_downloaded:
			log_write("Список тем доски %s страница %s уже скачен, обработка..." % (board, cur_page_num))
		else:
			log_write("Обработка...")
		threads_data = file_JSON_read(board_threads_fname)
		if not threads_data:
			get_threads_list(board, cur_page_num)
		# а вдруг ошибка (а они бывают, учитывая что доска может быть уже удалена вместе с темами)
		if 'threads' in threads_data.keys():
			cur_cursor = threads_data['pagination']['cursor']
			if cur_page_num > 1:
				log_write("Текущий курсор: " + cur_cursor)
			else:
				log_write("Получен курсор: " + cur_cursor)
			if len(threads_data['threads']):
				for threads in threads_data['threads']:
					get_thread(board, threads['thread']['id'])

				if threads_data['pagination']['hasMore']:
					log_write("Есть ещё страницы.")
					get_threads_list(board, cur_page_num + 1)
				else:
					log_write("Нету больше страниц.")
			else:
				log_write("В доске нет тем.")
		else:
			log_write("Отсутствует ключ threads, данные:")
			log_write(str(threads_data))
			cur_cursor = ""
			log_write("Курсор сброшен.")
	else:
		log_write("Скачиваем темы доски %s страница %s..." % (board, cur_page_num))
		if cur_page_num > 1:
			page_param = "&page=" + str(cur_page_num)
			if cur_cursor == "":
				log_write("Ошибка, курсор не установлен для последующих страниц! Аварийный выход.")
				log_write("Файл: " + board_threads_fname)
				sys.exit(1)
			cursor_param = '&cursor=' + cur_cursor
		else:
			page_param = ""
			cursor_param = ""
		link = API_URL + 'board?dir=' + board + page_param + cursor_param + '&session=' + SESSION
		log_write("Скачивание: " + link, "")
		request = requests.get(link)
		if request_ok(request):
			file_write(board_threads_fname, request.text)
			log_write(" закончено. ", "")
		get_threads_list(board, cur_page_num, True)

# Получение досок
def get_boards(BOARDS, now_downloaded=False):
	if path.isfile(BOARDS_FILENAME) and BOARDS == "auto":
		if not now_downloaded:
			log_write("Файл борд уже скачен, обработка...")
		else:
			log_write("Обработка...")
		boards_data = file_JSON_read(BOARDS_FILENAME)
		if not boards_data:
			get_boards(BOARDS)
		for hidden_board in HIDDEN_BOARDS:
			boards_data['boards'].append({'dir': hidden_board})
		for board in boards_data['boards']:
			boards_pages_logic(board['dir'], now_downloaded)
	elif BOARDS == "auto":
		log_write("Скачиваем файл досок...")
		link = API_URL + 'board/list?session=' + SESSION
		log_write("Скачивание: " + link, "")
		request = requests.get(link)
		if request_ok(request):
			file_write(BOARDS_FILENAME, request.text)
			log_write(" закончено. ", "")
		get_boards(BOARDS, True)
	elif type(BOARDS) == list:
		boards_data = {'boards': []}
		for board in BOARDS:
			boards_data['boards'].append({'dir': board})
		for hidden_board in HIDDEN_BOARDS:
			boards_data['boards'].append({'dir': hidden_board})
		for board in boards_data['boards']:
			boards_pages_logic(board['dir'], now_downloaded)
	else:
		log_write("Вы написали какую-то хуйню в BOARDS, возможно вы забыли вложить строку в массив?")

# Часть логики со страницами доски
def boards_pages_logic(board, now_downloaded):
		log_write("Обходим доски...")
		# не смотрим на порядок выдачи, ибо он может отличаться в зависимости от ОС, нам нужно только число файлов
		board_dir = path.join(ROOT_DIR, BOARDS_DIR, board)
		board_threads_glob = path.join(board_dir, 'threads*.json')
		board_threads_count = len(glob.glob(board_threads_glob))
		if not now_downloaded:
			log_write("Уже скаченное число страниц доски %s: %s" % (board, board_threads_count))
		if board_threads_count < 1:
			board_threads_count = 1  # скачаем, хуле
			log_write("Скаченных страниц не найдено. Скачиваем...")
		log_write("Текущая: %s" % board_threads_count)
		board_threads_filename = path.join(board_dir, 'threads-page%s.json' % board_threads_count)
		log_write("Путь: " + board_threads_filename)
		get_threads_list(board, board_threads_count)

# Обработка ошибок
def request_ok(request):
	if request.status_code == 521:
		log_write("Ошибка! Сервер отдаёт 521 Web server is down (CloudFlare).")
		log_write("Повторная попытка...")
		return False
	elif request.status_code == 502:
		log_write("Ошибка! Сервер отдаёт 502 Bad gateway.")
		log_write("Повторная попытка...")
		return False
	elif request.status_code == 504:
		log_write("Ошибка! Сервер отдаёт 503 Gateway time-out.")
		log_write("Повторная попытка...")
		return False
	elif request.status_code == 410:
		log_write("Ошибка! Сервер отдаёт 410 Gone.")
		log_write("Картинка протухла, удалите протухшие JSON файлы и продолжите...")
		log_write("Аварийное завершение.")
		sys.exit(1)
	else:
		return True

# Получение сессии
def get_session():
	if not 'SESSION' in globals():
		url = API_URL + 'session'
		log_write("Скачивание: " + url)
		request = requests.get(url)
		if request_ok(request):
			log_write(" закончено. ")
			return request.headers['X-Session']
	else:
		return SESSION


# Начать работу
if LOG_FILE_WRITE:
	log_file = log_start()
if IMAGES_SAVE:
	images_data = images_start()

SESSION = get_session()
get_boards(BOARDS) 
#get_thread("b", "1904") # можно скачать отдельную тему

# Работа закончена
log_write("Закончено.")
"""
if IMAGES_SAVE:
	with open(IMAGES_FILENAME, 'w') as images_file:
		json.dump(images_data, images_file, ensure_ascii=False)
"""
if LOG_FILE_WRITE:
	log_file.close()
